# store.
## online store for Scandiweb junior web developer assignment

Repository consists of 2 main folders:

1. SQL database dump file folder, which needs to be imported in a new database
2. www folder, which consists of website's files and folders

Website has been tested on:

* **Google Chrome** (Version 70.0.3538.102)
* **Mozilla Firefox Developer Edition** (64.0b10)
* **Safari** (12.0.1)
* **Opera** (6.0.3051.104)

**_This website is not optimized for Internet Explorer, Edge browsers and mobile devices in it's current state_**

## Working example of this project can be seen [here](http://projects.kristapsnaglis.id.lv/onlineshop)
