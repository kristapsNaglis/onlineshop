-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2018 at 12:26 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scandiwebtest`
--

-- --------------------------------------------------------

--
-- Table structure for table `deco`
--

CREATE TABLE `deco` (
  `id` int(11) NOT NULL,
  `sku` varchar(11) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `dimensions` varchar(10) NOT NULL,
  `price` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deco`
--

INSERT INTO `deco` (`id`, `sku`, `model_name`, `dimensions`, `price`, `img`) VALUES
(1, '75001', 'GRADVIS vase', '10x21x10', 7, 'images/thirdSection/gradvis_vase.png'),
(2, '75002', 'SJOPENNA lamp', '30x100x35', 25, 'images/thirdSection/sjopenna_lamp.png'),
(3, '75003', 'EKET cabinet', '35x35x25', 15, 'images/thirdSection/eket_cabinet.png'),
(12, '75004', 'GODOGON mirror', '50x50x3', 349, 'images/thirdSection/godogon_mirror.png');

--
-- Triggers `deco`
--
DELIMITER $$
CREATE TRIGGER `decorationLimitMax` BEFORE INSERT ON `deco` FOR EACH ROW BEGIN
  SELECT COUNT(*) INTO @cnt FROM deco;
  IF @cnt >= 4 THEN
    CALL sth(); -- raise an error
  END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `iphone`
--

CREATE TABLE `iphone` (
  `id` int(11) NOT NULL,
  `sku` varchar(11) NOT NULL,
  `model_name` varchar(100) NOT NULL,
  `size` int(11) NOT NULL,
  `price` double(11,0) NOT NULL,
  `img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iphone`
--

INSERT INTO `iphone` (`id`, `sku`, `model_name`, `size`, `price`, `img`) VALUES
(2, '19239385', 'iPhone X', 128, 1099, 'images/firstSection/iphoneX.png'),
(3, '19239386', 'iPhone Xr', 128, 1199, 'images/firstSection/iphoneXr.png'),
(22, '19239387', 'iPhone Xs', 256, 1299, 'images/firstSection/iphoneXs.png'),
(69, '19239387', 'iPhone 8', 64, 899, 'images/firstSection/iphone8.png');

--
-- Triggers `iphone`
--
DELIMITER $$
CREATE TRIGGER `iphoneLimitMax` BEFORE INSERT ON `iphone` FOR EACH ROW BEGIN
  SELECT COUNT(*) INTO @cnt FROM iphone;
  IF @cnt >= 4 THEN
    CALL sth(); -- raise an error
  END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `snowboards`
--

CREATE TABLE `snowboards` (
  `id` int(11) NOT NULL,
  `sku` int(11) NOT NULL,
  `model_name` text NOT NULL,
  `weight` int(11) NOT NULL,
  `price` int(99) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `snowboards`
--

INSERT INTO `snowboards` (`id`, `sku`, `model_name`, `weight`, `price`, `img`) VALUES
(1, 15001, 'Burton Feelgood', 1, 549, 'images/secondSection/burt_feelgood.png'),
(2, 15002, 'Butron Stylus', 2, 359, 'images/secondSection/burt_Stylus.png'),
(3, 15003, 'Lib Tec Skunk', 1, 609, 'images/secondSection/libtec_sk.png'),
(15, 15004, 'Arbor Formula', 1, 349, 'images/secondSection/arbor.png');

--
-- Triggers `snowboards`
--
DELIMITER $$
CREATE TRIGGER `snowboardsLimitMax` BEFORE INSERT ON `snowboards` FOR EACH ROW BEGIN
  SELECT COUNT(*) INTO @cnt FROM snowboards;
  IF @cnt >= 4 THEN
    CALL sth(); -- raise an error
  END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `deco`
--
ALTER TABLE `deco`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `iphone`
--
ALTER TABLE `iphone`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `snowboards`
--
ALTER TABLE `snowboards`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `deco`
--
ALTER TABLE `deco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `iphone`
--
ALTER TABLE `iphone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `snowboards`
--
ALTER TABLE `snowboards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
