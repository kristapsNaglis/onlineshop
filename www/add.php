<?php
include_once "includes/IPhone.inc";
include_once "includes/Snowboard.inc";
include_once "includes/Decoration.inc";
include "includes/Submit.inc";
?>

<!--Start HTML code-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>store. | add products</title>

    <!--Import here external css, javascript, fonts and other files-->
    <link rel="stylesheet" href="css/mainStyle.css">
    <link rel="stylesheet" href="css/addStyle.css">
    <link rel="stylesheet" href="css/submitTableStyle.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/onChangeToggleVisibility.js"></script>
    <script src="js/ToggleFormElements.js"></script>
    <script src="js/FormValidation.js"></script>
    <script src="js/GetInfoFromForm.js"></script>
    <script src="js/DatabaseTableSelect.js"></script>
</head>
<body>

<?php
/*Create new Submit class object*/
$submit = new Submit();

/*Create connection with getIphone class*/
$iphone = new getIphone();
/*Get query statement to select all iphones form database*/
$iphoneStatement = $iphone->getStatement();

/*Create connection with getSnowboard class*/
$snowboard = new getSnowboard();
/*Get query statement to select all snowboards form database*/
$snowboardStatement = $snowboard->getStatement();

/*Create connection with getDecoration class*/
$decoration = new getDecoration();
/*Get query statement to select all decoration form database*/
$decorationStatement = $decoration->getStatement();
?>

<!--Creating a container that holds everything on the page-->
<div id="container">
    <!--Container for heading part of webpage-->
    <div id="heading">
        <!--Left container of heading-->
        <div id="left-logo">
            <div id="mainTextLogoContainer">
                <div id="mainTextLogo">store.</div>
            </div>
            <div id="classTextContainer">
                <div id="classText"><p>add products</p></div>
            </div>
        </div>
        <!--Center container of heading. More info can be placed here-->
        <div id="center"></div>
        <!--Right container of heading-->
        <div id="right-menu">
            <div id="navbarContainer">
                <ul id="navbar">
                    <!--Navigation bar selection-->
                    <li><a href="index.php">product list</a></li>
                </ul>
            </div>
        </div>
    </div>

    <!--Container for holding all the content underneath heading-->
    <div id="mainContentContainer">
        <!--Left column container - Form-->
        <div id="formDiv">
            <!--Form-->
            <form action="formSubmitted.php" method="post" id="form" autocomplete="off"
                  onsubmit="return new FormValidation().validateForm()">
                <p class="label">SKU:</p>
                <input type="text" name="sku" id="sku">
                <br>
                <p class="label">Name:</p>
                <input type="text" name="name" id="name">
                <br>
                <p class="label">Price:</p>
                <input type="text" name="price" id="price">
                <br>
                <p class="label">Type Switcher (select an option)</p>
                <select name="typeSwitcher" id="typeSwitcher">
                    <option id="opt1" value="<?php $submit->getVarForTypeSwitcher(1) ?>">iPhone</option>
                    <option id="opt2" value="<?php $submit->getVarForTypeSwitcher(2) ?>">snowboards</option>
                    <option id="opt3" value="<?php $submit->getVarForTypeSwitcher(3) ?>">decoration</option>
                </select>
                <br>
                <div id="varContent">
                    <div id="iphoneSelect">
                        <p class="label">Size:</p>
                        <input type="text" name="size" id="size">
                    </div>
                    <br>
                    <div id="snowboardSelect" hidden>
                        <p class="label">Weight:</p>
                        <input type="text" name="weight" id="weight">
                    </div>
                    <br>
                    <div id="decoSelect" hidden>
                        <p class="label">Height:</p>
                        <input type="text" name="height" id="height">
                        <br>
                        <p class="label">Width:</p>
                        <input type="text" name="width" id="width">
                        <br>
                        <p class="label">Length:</p>
                        <input type="text" name="length" id="length">
                        <br>
                    </div>
                </div>
                <p class="label">Image path:</p>
                <input type="text" name="img" id="img" value="images/">
                <br>
                <div id="submitButtonDiv">
                    <input class="submitButton" type="submit" title="submit" value="Submit to database">
                </div>
            </form>
        </div>

        <!--Right column container - Table-->
        <div id="rightSide">
            <!--Table-->
            <div id="phpTable"></div>
        </div>
    </div>
</div>

<!--Javascript's that need to be initialized after html-->
<script src="js/deleteRow.js"></script>
</body>
</html>
