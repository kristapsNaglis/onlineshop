<?php
include "includes/Submit.inc";

/*Create new Submit class object*/
$classSubmit = new Submit();

/*
 * Get all three options from select menu
 * and store them in 3 different variables
*/
$var1 = $classSubmit->getVar1();
$var2 = $classSubmit->getVar2();
$var3 = $classSubmit->getVar3();

/*Get the selected value by user*/
$typeSwitcherValue = $_POST['typeSwitcher'];

/*
 * Compare user selected value
 * with all the options
*/
if ($typeSwitcherValue == $var1) {
    $classSubmit->submitVar1();
}
if ($typeSwitcherValue == $var2) {
    $classSubmit->submitVar2();
}
if ($typeSwitcherValue == $var3) {
    $classSubmit->submitVar3();
}
?>

<!--Start HTML code-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>store. | add products</title>

    <!--Import here external css, javascript, fonts and other files-->
    <link rel="stylesheet" href="css/mainStyle.css">
    <link rel="stylesheet" href="css/formSubmittedStyle.css">
</head>
<body>

<!--Container for text-->
<div id="goBackButtonDiv">
    <a id="goBackButton" href="add.php">CLICK HERE TO GO BACK</a>
</div>
</body>
</html>