<?php

/*Create class*/

class DatabaseConn {
    /*Declare variables for PDO*/
    private $serverName;
    private $userName;
    private $password;
    private $dbName;
    private $charset;

    /*Function to establish connection with database*/
    public function connect() {
        /*Assign values to variables*/
        $this->serverName = "localhost";
        $this->userName = "root";
        $this->password = "";
        $this->dbName = "scandiwebTest";
        $this->charset = "utf8mb4";

        try {
            /*PDO connection*/
            $dsn = "mysql:host=" . $this->serverName . ";dbname=" . $this->dbName . ";charset=" . $this->charset;
            $pdo = new PDO($dsn, $this->userName, $this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (Exception $exception) {
            /*Error message*/
            echo "Connection failed: " . $exception->getMessage();
        }
    }
}
