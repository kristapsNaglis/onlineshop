<?php

/*Create class that extends connection to database
and gets all the info from decoration table*/

class getDecoration extends DatabaseConn {
    /*Declare variables*/
    public $table = 'deco';
    public $query;

    /*Constructor to create query when class object is created*/
    public function __construct() {
        $this->setQuery();
    }

    /*Getter functions*/
    public function getQuery() {
        $query = $this->query;
        return $query;
    }

    /*Setter function*/
    public function setQuery() {
        $this->query = "SELECT * FROM $this->table";
    }

    public function getStatement() {
        $statement = $this->connect()->query($this->getQuery());
        return $statement;
    }
}

/*Create class that
sets all the info from decoration table*/

class setDecoration {
    /*Declare variables*/
    protected $id;
    protected $sku;
    protected $model_name;
    protected $dimensions;
    protected $price;
    protected $img;

    public $query;

    /*Constructor to create query when class object is created*/
    public function __construct() {
        $this->setQuery();
    }

    /*Setter function*/
    public function setQuery() {
        $this->query = "INSERT INTO deco (id, sku, model_name, dimensions, price, img) VALUES (?,?,?,?,?,?)";
    }

    /*Getter function*/
    public function getQuery() {
        $query = $this->query;
        return $query;
    }

    /*Function to convert dimensions from separate variables into one
    with correct formatting*/
    public function convertDimensions() {
        $height = $_POST['height'];
        $width = $_POST['width'];
        $length = $_POST['length'];

        $final = $height . "x" . $width . "x" . $length;

        return $final;
    }

    /*Get values from chosen column*/
    public function getFormValuesDeco($whichColumn) {
        switch ($whichColumn) {
            case 1:
                return 'DEFAULT';
                break;
            case 2:
                return $_POST['sku'];
                break;
            case 3:
                return $_POST['name'];
                break;
            case 4:
                return $this->convertDimensions();
                break;
            case 5:
                return $_POST['price'];
                break;
            case 6:
                return $_POST['img'];
                break;
            default:
                echo "Please input a valid value";
                break;
        }
    }
}