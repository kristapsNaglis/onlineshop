<?php
include 'DatabaseConn.inc';

/*Create class that extends connection to database
and gets all the info from iphone table*/

class getIphone extends DatabaseConn {
    /*Declare variables*/
    public $table = 'iphone';
    public $query;

    /*Constructor to create query when class object is created*/
    public function __construct() {
        $this->setQuery();
    }

    /*Getter functions*/
    public function getQuery() {
        $query = $this->query;
        return $query;
    }

    public function getStatement() {
        $statement = $this->connect()->query($this->getQuery());
        return $statement;
    }

    /*Setter function*/
    public function setQuery() {
        $this->query = "SELECT * FROM $this->table";
    }
}

/*Create class that
sets all the info from decoration table*/

class setIphone extends DatabaseConn {
    /*Declare variables*/
    public $id;
    public $sku;
    public $model_name;
    public $size;
    public $price;
    public $img;

    public $query;

    /*Constructor to create query when class object is created*/
    public function __construct() {
        $this->setQuery();
    }

    /*Setter function*/
    public function setQuery() {
        $this->query = "INSERT INTO iphone (id, sku, model_name, size, price, img) VALUES (?,?,?,?,?,?)";
    }

    /*Getter function*/
    public function getQuery() {
        $query = $this->query;
        return $query;
    }

    /*Get values from chosen column*/
    public function getFormValuesIphone($whichColumn) {
        switch ($whichColumn) {
            case 1:
                return 'DEFAULT';
                break;
            case 2:
                return $_POST['sku'];
                break;
            case 3:
                return $_POST['name'];
                break;
            case 4:
                return $_POST['size'];
                break;
            case 5:
                return $_POST['price'];
                break;
            case 6:
                return $_POST['img'];
                break;
            default:
                echo "Please input a valid value";
                break;
        }
    }
}