<?php
include_once 'DatabaseConn.inc';

/*Create class*/

class PassDBTables {
    public $table;

    /*Constructor that sets table variable when new class
    object is created*/
    function __construct() {
        $this->setTable();
    }

    /*Getter function*/
    public function getTable() {
        return $this->table;
    }

    /*Setter function*/
    public function setTable() {
        $this->table = $table = $_GET['table'];
    }

    /*Function to display table*/
    function displayTable() {

        /*Create connection to database*/
        $link = new DatabaseConn();
        $pdo = $link->connect();

        /*Switch operator to decide which table to display*/
        switch ($this->getTable()) {
            case 'iphone':
                /*Prepare and execute query statement to show table*/
                $stmt = $pdo->prepare("SELECT * FROM iphone");
                $stmt->execute();

                /*Echo HTML table with variables from table*/
                echo "<table>
                <tr>
                <th>SKU</th>
                <th>Model name</th>
                <th>Price</th>
                <th>Size</th>
                <th>Image path</th>
                </tr>";
                /*While loop to iterate through all database rows
                 and create table rows*/
                while ($row = $stmt->fetch(PDO::FETCH_BOTH)) {
                    echo "<tr>";
                    echo "<td>" . $row['sku'] . "</td>";
                    echo "<td>" . $row['model_name'] . "</td>";
                    echo "<td>" . $row['price'] . "</td>";
                    echo "<td>" . $row['size'] . "</td>";
                    echo "<td>" . $row['img'] . "</td>";
                    /*Delete button with JS function to delete
                    according row from table*/
                    echo "<td class='delete'><div onclick='new DeleteRow().delete(";
                    echo $row['id'] . ") ' 
            class='deleteButton'";
                    echo $row['id'] . "'>Delete</td>";
                    echo "</tr>";
                }
                echo "</table>";
                break;

            case 'snowboards':
                /*Prepare and execute query statement to show table*/
                $stmt = $pdo->prepare("SELECT * FROM snowboards");
                $stmt->execute();

                /*Echo HTML table with variables from table*/
                echo "<table>
                <tr>
                <th>SKU</th>
                <th>Model name</th>
                <th>Price</th>
                <th>Weight</th>
                <th>Image path</th>
                </tr>";
                /*While loop to iterate through all database rows
                  and create table rows*/
                while ($row = $stmt->fetch(PDO::FETCH_BOTH)) {
                    echo "<tr>";
                    echo "<td>" . $row['sku'] . "</td>";
                    echo "<td>" . $row['model_name'] . "</td>";
                    echo "<td>" . $row['price'] . "</td>";
                    echo "<td>" . $row['weight'] . "</td>";
                    echo "<td>" . $row['img'] . "</td>";
                    /*Delete button with JS function to delete
                    according row from table*/
                    echo "<td class='delete'><div onclick='new DeleteRow().delete(";
                    echo $row['id'] . ")' 
            class='deleteButton' id='";
                    echo $row['id'] . "'>Delete</td>";
                    echo "</tr>";
                }
                echo "</table>";
                break;

            case 'deco':
                /*Prepare and execute query statement to show table*/
                $stmt = $pdo->prepare("SELECT * FROM deco");
                $stmt->execute();

                /*Echo HTML table with variables from table*/
                echo "<table>
                <tr>
                <th>SKU</th>
                <th>Model name</th>
                <th>Price</th>
                <th>Dimensions</th>
                <th>Image path</th>
                </tr>";
                /*While loop to iterate through all database rows
                  and create table rows*/
                while ($row = $stmt->fetch(PDO::FETCH_BOTH)) {
                    echo "<tr>";
                    echo "<td>" . $row['sku'] . "</td>";
                    echo "<td>" . $row['model_name'] . "</td>";
                    echo "<td>" . $row['price'] . "</td>";
                    echo "<td>" . $row['dimensions'] . "</td>";
                    echo "<td>" . $row['img'] . "</td>";
                    /*Delete button with JS function to delete
                    according row from table*/
                    echo "<td class='delete'><div onclick='new DeleteRow().delete(";
                    echo $row['id'] . ")' 
            class='deleteButton' id='";
                    echo $row['id'] . "'>Delete</td>";
                    echo "</tr>";
                }
                echo "</table>";
                break;

            default:
                echo "No table was selected";
                break;
        }
    }

    /*Function to delete row
    Called by JS when clicking on Delete button*/
    function deleteRowIphone() {
        /*Declare variables*/
        $deleteId = $_GET['delete_id'];
        $switcherValue = $_GET['switcherValue'];
        /*Create connection with database*/
        $link = new DatabaseConn();
        $pdo = $link->connect();
        /*Execute delete*/
        $stmt = $pdo->prepare("delete from $switcherValue where id = $deleteId");
        $stmt->execute();
    }
}