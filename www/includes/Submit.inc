<?php
include_once "IPhone.inc";
include_once "Snowboard.inc";
include_once "Decoration.inc";

class Submit {
    /*Database connection object*/
    public $connection;
    /**
     * Variables
     **/
    /*variables that change depending on what type products will be featured*/
    public $var1 = "iphone";
    public $var2 = "snowboards";
    public $var3 = "deco";
    /*variables for every type*/
    public $sku;
    public $name;
    public $price;
    public $switcher;
    public $img;
    /*variables only for iphone type*/
    public $size;
    /*variables only for snowboards type*/
    public $weight;
    /*variables only for decoration type*/
    public $height;
    public $width;
    public $length;

    /*selector value*/
    public $selectedSwitcherValue;

    /**
     * Functions
     **/
    public function establishDatabaseConnection() {
        $v = new DatabaseConn();
        $this->connection = $v->connect();
        return $this->connection;
    }

    /*Setter functions*/
    public function setSetIphone() {
        $iphone = new setIphone();
        return $iphone;
    }

    public function setSetSnowboards() {
        $snowboards = new setSnowboard();
        return $snowboards;
    }
    public function setSetDecoration() {
        $deco = new setDecoration();
        return $deco;
    }

    /*Getter functions*/
    public function getVar1() {
        return $this->var1;
    }

    public function getVar2() {
        return $this->var2;
    }

    public function getVar3() {
        return $this->var3;
    }

    /*Get values from chosen variable*/
    public function getVarForTypeSwitcher($whichVarNeeded) {
        switch ($whichVarNeeded) {
            case 1:
                echo $this->var1;
                break;
            case 2:
                echo $this->var2;
                break;
            case 3:
                echo $this->var3;
                break;
            default:
                echo "Incorrect value inputted";
        }
    }

    /*Functions to submit data from form to database*/
    public function submitVar1() {
        try {
            /*Get query from iphone class*/
            $sql = $this->setSetIphone()->getQuery();
            /*Submit data to database*/
            $this->establishDatabaseConnection()->prepare($sql)->execute([
                $this->setSetIphone()->getFormValuesIphone(1),
                $this->setSetIphone()->getFormValuesIphone(2),
                $this->setSetIphone()->getFormValuesIphone(3),
                $this->setSetIphone()->getFormValuesIphone(4),
                $this->setSetIphone()->getFormValuesIphone(5),
                $this->setSetIphone()->getFormValuesIphone(6)
            ]);
            /*Echo if successful*/
            echo "<div class='submitResultContainer'> <p class='submitResult'> Submit successful </p>";
        } catch (Exception $e) {
            /*Echo if unsuccessful*/
            echo "<div class='submitResultContainer'> <p class='submitResult'> Exception was made: 
            Max number of items is 4. 
            Please delete older items, to be able to insert new ones </p>";
        }
    }
    public function submitVar2() {
        try {
            /*Get query from snowboard class*/
            $sql = $this->setSetSnowboards()->getQuery();
            /*Submit data to database*/
            $this->establishDatabaseConnection()->prepare($sql)->execute([
                $this->setSetSnowboards()->getFormValuesSnowboard(1),
                $this->setSetSnowboards()->getFormValuesSnowboard(2),
                $this->setSetSnowboards()->getFormValuesSnowboard(3),
                $this->setSetSnowboards()->getFormValuesSnowboard(4),
                $this->setSetSnowboards()->getFormValuesSnowboard(5),
                $this->setSetSnowboards()->getFormValuesSnowboard(6)
            ]);
            /*Echo if successful*/
            echo "<div class='submitResultContainer'> <p class='submitResult'> Submit successful </p>";
        } catch (Exception $e) {
            /*Echo if unsuccessful*/
            echo "<div class='submitResultContainer'> <p class='submitResult'> Exception was made: 
            Max number of items is 4. 
            Please delete older items, to be able to insert new ones </p>";
        }
    }
    public function submitVar3() {
        try {
            /*Get query from snowboard class*/
            $sql = $this->setSetDecoration()->getQuery();
            /*Submit data to database*/
            $this->establishDatabaseConnection()->prepare($sql)->execute([
                $this->setSetDecoration()->getFormValuesDeco(1),
                $this->setSetDecoration()->getFormValuesDeco(2),
                $this->setSetDecoration()->getFormValuesDeco(3),
                $this->setSetDecoration()->getFormValuesDeco(4),
                $this->setSetDecoration()->getFormValuesDeco(5),
                $this->setSetDecoration()->getFormValuesDeco(6)
            ]);
            /*Echo if successful*/
            echo "<div class='submitResultContainer'> <p class='submitResult'> Submit successful </p>";
        } catch (Exception $e) {
            /*Echo if unsuccessful*/
            echo "<div class='submitResultContainer'> <p class='submitResult'> Exception was made: 
            Max number of items is 4. 
            Please delete older items, to be able to insert new ones </p></div>";
        }
    }

}