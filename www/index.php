<?php
include "includes/IPhone.inc";
include "includes/Snowboard.inc";
include "includes/Decoration.inc";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>store. | product List</title>

    <link rel="stylesheet" href="css/mainStyle.css">
    <link rel="stylesheet" href="css/indexStyle.css">
</head>
<body>

<?php
/*Create a connection with database*/
$conn = new DatabaseConn();
$conn = $conn->connect();

/*Create connection with getIphone class*/
$iphone = new getIphone();
/*Get query statement to select all iphones form database*/
$iphoneStatement = $iphone->getStatement();

/*Create connection with getSnowboard class*/
$snowboard = new getSnowboard();
/*Get query statement to select all snowboards form database*/
$snowboardStatement = $snowboard->getStatement();

/*Create connection with getDecoration class*/
$decoration = new getDecoration();
/*Get query statement to select all decoration form database*/
$decorationStatement = $decoration->getStatement();
?>

<!--Creating a container that holds everything on the page-->
<div id="container">
    <!--Container for heading part of webpage-->
    <div id="heading">
        <!--Left container of heading-->
        <div id="left-logo">
            <div id="mainTextLogoContainer">
                <div id="mainTextLogo">store.</div>
            </div>
            <div id="classTextContainer">
                <div id="classText"><p>product list</p></div>
            </div>
        </div>
        <!--Center container of heading. More info can be placed here-->
        <div id="center"></div>
        <!--Right container of heading-->
        <div id="right-menu">
            <div id="navbarContainer">
                <ul id="navbar">
                    <!--Navigation bar selection-->
                    <li><a href="#sectionAnchor">iPhone</a></li>
                    <li><a href="#secondSectionAnchor">snowboards</a></li>
                    <li><a href="#thirdSectionAnchor">home decoration</a></li>
                    <li><a href="add.php">add products</a></li>
                </ul>
            </div>
        </div>
    </div>

    <!--Container for holding all the content underneath heading-->
    <div id="mainContentContainer">
        <!--First section container-->
        <div id="section">
            <!--Header for first section-->
            <div id="sectionHeaderContainer">
                <!--Apply background image to div-->
                <div id="sectionHeaderImage" style="background: url('images/firstSection/appleHeader.png') no-repeat 50% 50%"></div>
                <div id="sectionHeaderText"><p id="sectionAnchor">new iPhone</p></div>
            </div>
            <!--Container for holding all the items for section 1-->
            <div id="sectionContentContainer">
                <!--PHP loop that iterates over all the rows from the table-->
                <?php while ($row = $iphoneStatement->fetch()): ?>
                <div class="sectionContent">
                    <div class="flipper">
                        <div class="front">
                            <div class="frontImageContainer">
                                <!--Apply background image to div-->
                                <div class="frontImageText" style="background: url('<?php echo $row['img']?>')">
                                    <div class="text">
                                        <!--Display model name of the item-->
                                        <?php echo $row['model_name'] ?>
                                    </div>
                                </div>
                                <div class="frontImage"></div>
                            </div>
                        </div>
                        <div class="back one">
                            <div class="backImageContainer">
                                <div class="backImageText">
                                    <!--Display other properties of the item-->
                                    <p>SKU: <?php echo $row['sku'] ?></p>
                                    <p>Size: <?php echo $row['size'] ?>GB</p>
                                    <p>Price: <?php echo $row['price'] ?>€</p>
                                </div>
                                <!--Apply background image to div-->
                                <div class="frontImage" style="background: url('<?php echo $row['img']?>')"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>

        <!--Second section container-->
        <div id="section">
            <!--Header for second section-->
            <div id="sectionHeaderContainer">
                <div id="sectionHeaderImage" style="background: url('images/secondSection/header.png') no-repeat 50% 50%"></div>
                <div id="sectionHeaderText"><p id="secondSectionAnchor">snowboards</p></div>
            </div>
            <!--Container for holding all the items for section 2-->
            <div id="sectionContentContainer">
                <!--PHP loop that iterates over all the rows from the table-->
                <?php while ($row = $snowboardStatement->fetch()): ?>
                    <div class="sectionContent one">
                        <div class="flipper">
                            <div class="front one">
                                <div class="frontImageContainer">
                                    <!--Apply background image to div-->
                                    <div class="frontImageText one" style="background: url('<?php echo $row['img']?>') no-repeat 50% 50%">
                                        <div class="text">
                                            <!--Display model name of the item-->
                                            <?php echo $row['model_name'] ?>
                                        </div>
                                    </div>
                                    <div class="frontImage"></div>
                                </div>
                            </div>
                            <div class="back one">
                                <div class="backImageContainer">
                                    <div class="backImageText">
                                        <p>SKU: <?php echo $row['sku'] ?></p>
                                        <p>Weight: <?php echo $row['weight'] ?>KG</p>
                                        <p>Price: <?php echo $row['price'] ?>€</p>
                                    </div>
                                    <!--Apply background image to div-->
                                    <div class="frontImage" style="background: url('<?php echo $row['img']?>') no-repeat 50% 50%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>

        <!--Third section container-->
        <div id="section">
            <!--Header for third section-->
            <div id="sectionHeaderContainer">
                <div id="sectionHeaderImage" style="background: url('images/thirdSection/header.png') no-repeat 50% 50%"></div>
                <div id="sectionHeaderText"><p id="thirdSectionAnchor">decoration</p></div>
            </div>
            <!--Container for holding all the items for section 3-->
            <div id="sectionContentContainer">
                <!--PHP loop that iterates over all the rows from the table-->
                <?php while ($row = $decorationStatement->fetch()): ?>
                    <div class="sectionContent one">
                        <div class="flipper">
                            <div class="front one">
                                <div class="frontImageContainer">
                                    <div class="frontImageText">
                                        <div class="text">
                                            <!--Display model name of the item-->
                                            <?php echo $row['model_name'] ?>
                                        </div>
                                    </div>
                                    <!--Apply background image to div-->
                                    <div class="frontImage" style="background: url('<?php echo $row['img']?>') no-repeat 50% 50%"></div>
                                </div>
                            </div>
                            <div class="back one">
                                <div class="backImageContainer">
                                    <div class="backImageText">
                                        <p>SKU: <?php echo $row['sku'] ?></p>
                                        <p>Dimensions: <?php echo $row['dimensions'] ?>cm</p>
                                        <p>Price: <?php echo $row['price'] ?>€</p>
                                    </div>
                                    <!--Apply background image to div-->
                                    <div class="frontImage" style="background: url('<?php echo $row['img']?>') no-repeat 50% 50%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</div>

</body>
</html>
