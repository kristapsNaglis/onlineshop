/*
* Class to display database based on user selected value
* by sending GET request to displayTable.php
*/
function DatabaseTableSelect() {
    /*Function*/
    this.showAccordingTable = function (switcherValue) {
        /*If there is no switcher value then return nothing*/
        if (switcherValue == "") {
            document.getElementById("phpTable").innerHTML = "";
            return;
        }

        if (window.XMLHttpRequest) {
            /*Code for IE7+, Firefox, Chrome, Opera, Safari*/
            xmlhttp = new XMLHttpRequest();
        } else { /*Code for IE6, IE5*/
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("phpTable").innerHTML = this.responseText;
            }
        };
        /*Submit GET request with user selected value*/
        xmlhttp.open("GET", "controller/displayTable.php?table=" + switcherValue);
        xmlhttp.send();
    }
}