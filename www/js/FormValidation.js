/*
* Class to validate all the form input fields,
* if the values are valid
*/

function FormValidation() {

    /*Define variables and assign values*/
    let formInfo = new GetInfoFromForm();

    let sku = formInfo.getSku();
    let name = formInfo.getName();
    let price = formInfo.getPrice();

    let switcherValue = formInfo.getSwitcherValue();
    let option1 = formInfo.getOpt1();
    let option2 = formInfo.getOpt2();
    let option3 = formInfo.getOpt3();

    let size = formInfo.getSize();
    let weight = formInfo.getWeight();
    let height = formInfo.getHeight();
    let width = formInfo.getWidth();
    let length = formInfo.getLength();

    let img = formInfo.getImg();

    /*Function that checks if form input fields are valid*/
    this.validateForm = function () {
        /*isNaN - check if numeric value*/
        /*Check SKU*/
        if (sku == "" || isNaN(sku) || sku < 1 || sku > 999999999) {
            alert("Please input a valid SKU code \n" +
                "Numeric value between 1 and 99999");
            return false;
        }
        /*Check NAME*/
        if (name == "" || name.length > 30) {
            alert("Please input a valid name value \n" +
                "Text that is not longer than 30 characters");
            return false;
        }
        /*Check PRICE*/
        if (price == "" || isNaN(price) || price < 0 || price > 99999) {
            alert("Please input valid price \n" +
                "Positive numeric value, that does not exceed 99999");
            return false;
        }
        switch (switcherValue) {
            case option1:
                /*Check SIZE*/
                if (size == "" || isNaN(size) || size < 16 || size > 80000) {
                    alert("Please input valid size in gigabytes \n" +
                        "Positive numeric value, that does not exceed 8 terabytes");
                    return false;
                }
                break;
            case option2:
                /*Check WEIGHT*/
                if (weight == "" || isNaN(weight) || weight < 1 || weight > 50) {
                    alert("Please input valid weight in kilograms \n" +
                        "Positive numeric value, that does not exceed 50 kilograms");
                    return false;
                }
                break;
            case option3:
                /*Check DIMENSIONS*/
                if (width == "" || height == "" || length == "" ||
                    isNaN(width) || width < 1 || width > 100 ||
                    isNaN(height) || height < 1 || height > 100 ||
                    isNaN(length) || length < 1 || length > 100
                ) {
                    alert("Please input valid dimensions in meters \n" +
                        "Positive numeric values, that does not exceed 100 meters each");
                    return false;
                }
        }
        /*Check IMAGE PATH*/
        if (img == "" || img.length > 99) {
            alert("Please input a valid image path \n" +
                "Text that is not longer than 99 characters");
            return false;
        }
    }
}