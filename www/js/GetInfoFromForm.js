/*
* Class to get all input field variables
*/

function GetInfoFromForm() {
    /*Define variables and assign values*/
    this.sku = $('#sku').val();
    this.name = $('#name').val();
    this.price = $('#price').val();

    this.typeSwitcher = $('#typeSwitcher').val();

    this.option1Value = $('#opt1').val();
    this.option2Value = $('#opt2').val();
    this.option3Value = $('#opt3').val();

    this.size = $('#size').val();

    this.weight = $('#weight').val();

    this.height = $('#height').val();
    this.width = $('#width').val();
    this.length = $('#length').val();

    this.img = $('#img').val();


    /*
    * Functions to fetch variables from form
    */
    /*Get sku, name and price variables*/
    this.getSku = function() {
        return this.sku;
    };
    this.getName = function () {
        return this.name;
    };
    this.getPrice = function () {
        return this.price;
    };
    /*Get type switcher selected option*/
    this.getSwitcherValue = function() {
        return this.typeSwitcher;
    };
    /*Get options from type switcher*/
    this.getOpt1 = function() {
        return this.option1Value;
    };

    this.getOpt2 = function() {
        return this.option2Value;
    };

    this.getOpt3 = function() {
        return this.option3Value;
    };
    /*Get form values if iphone is selected*/
    this.getSize = function() {
        return this.size;
    };
    /*Get form values if snowboards is selected*/
    this.getWeight = function() {
        return this.weight;
    };
    /*Get form values if decoration is selected*/
    this.getHeight = function() {
        return this.height;
    };
    this.getWidth = function() {
        return this.width;
    };
    this.getLength = function() {
        return this.length;
    };
    this.getImg = function() {
        return this.img;
    };

}