/*
* Class to toggle the visibility of form
* elements accordingly to what user selected
*/

function ToggleFormElements() {

    /*Define variables and assign values*/
    let formInfo = new GetInfoFromForm();

    let switcherValue = formInfo.getSwitcherValue();
    let option1 = formInfo.getOpt1();
    let option2 = formInfo.getOpt2();
    let option3 = formInfo.getOpt3();

    let iphoneSelect = $('#iphoneSelect');
    let snowboardSelect = $('#snowboardSelect');
    let decoSelect = $('#decoSelect');


    let size = $('#size');
    let weight = $('#weight');
    let height = $('#height');
    let width = $('#width');
    let length = $('#length');


    /*Function to toggle visibility of form elements*/
    this.toggleSwitch = function () {
        switch (switcherValue) {
            case option1:
                iphoneSelect.show();
                snowboardSelect.hide();
                decoSelect.hide();

                weight.val('');

                height.val('');
                width.val('');
                length.val('');
                break;
            case option2:
                iphoneSelect.hide();
                snowboardSelect.show();
                decoSelect.hide();

                size.val('');

                height.val('');
                width.val('');
                length.val('');
                break;
            case option3:
                iphoneSelect.hide();
                snowboardSelect.hide();
                decoSelect.show();

                size.val('');
                weight.val('');
        }
    };
}