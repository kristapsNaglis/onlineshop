/*Class to delete row*/

function DeleteRow() {

    /*Define variable*/
    let formInfo = new GetInfoFromForm();
    let switcherValue = formInfo.getSwitcherValue();

    /*
    * AJAX GET request submitting to deleteTable.php
    * with variables
    */
    this.delete = function (deleteId) {
        $.ajax({
            type: 'GET',
            url: 'controller/deleteTable.php',
            data: 'delete_id=' + deleteId + "&switcherValue=" + switcherValue,

            /*Automatically reload page if request was successful*/
            success: function () {
                location.reload();
            }
        });
    };
}