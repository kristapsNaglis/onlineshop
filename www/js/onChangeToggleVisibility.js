/*
* Changing visibility of form elements and tables
* according to what switcher value user selected
*/
$(document).ready(function () {

    /*Show default table when page has loaded*/
    let switcherValue = new GetInfoFromForm().getSwitcherValue();
    new DatabaseTableSelect().showAccordingTable(switcherValue);

    /*Run when #typeSwitcher value changes*/
    $('#typeSwitcher').change(function () {
        /*Display according form elements*/
        let switcherValue = new GetInfoFromForm().getSwitcherValue();
        new DatabaseTableSelect().showAccordingTable(switcherValue);
        /*Display according table*/
        let toggleVisibility = new ToggleFormElements();
        toggleVisibility.toggleSwitch();
    });
});

